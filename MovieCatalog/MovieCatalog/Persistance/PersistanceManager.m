//
//  PersistanceManager.m
//  MovieCatalog
//
//  Created by Sunil Phani Manne on 16/01/19.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import "PersistanceManager.h"

#import "Constants.h"

@interface PersistanceManager()

@property (nonatomic, strong) NSString *testNameSpacePrefix;

@end

@implementation PersistanceManager

+ (instancetype)sharedInstance
{
    static PersistanceManager *sharedInstance = nil;
    static dispatch_once_t playerManagerToken;
    dispatch_once(&playerManagerToken, ^{
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
}

+ (instancetype)sharedManager
{
    return [self sharedInstance];
}

- (void)persistObject:(id <PersistableObjectProtocol>)object forKey:(NSString *)key
{
    NSData* encodedData = [NSKeyedArchiver archivedDataWithRootObject:object];
    [[NSUserDefaults standardUserDefaults] setObject:encodedData
                                              forKey:[self effectiveKeyForKey:key]];
}

- (id)retrieveObjectForKey:(NSString *)key
{
    id object = nil;
    
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:[self effectiveKeyForKey:key]];
    if (data) {
        object = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    }
    
    return object;
}

- (id)loadInstanceForClass:(Class)class andKey:(NSString *)key
{
    //Try to retrieve it from the persistence store...
    id <PersistableObjectProtocol> object = [self retrieveObjectForKey:[self effectiveKeyForKey:key]];
    
    //...if not, create a new EMPTY one!
    if (!object) {
        object = [[class alloc] init];
    }
    
    return object;
}

#pragma mark - Unit testing helper Methods
- (NSString *)effectiveKeyForKey:(NSString *)key
{
    NSString *effectiveKey = key;
    
    if (key == nil) {
        return nil;
    }
    
    if (_testNameSpacePrefix) {
        effectiveKey = [NSString stringWithFormat:@"%@%@%@",
                        _testNameSpacePrefix,
                        PersistanceManagerTestPrefixSeparator,
                        key];
    }
    
    return effectiveKey;
}

@end
