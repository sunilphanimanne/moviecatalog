//
//  PersistanceManager+Test.h
//  MovieCatalog
//
//  Created by Sunil Phani Manne on 16/01/19.
//  Copyright © 2019 Sunil Phani Manne. All rights reserved.
//

#import "PersistanceManager.h"

#ifndef PersistanceManager_Test_h
#define PersistanceManager_Test_h

@interface PersistanceManager (Test)

#pragma mark - Unit testing helper Methods and Properties

/*!
 Property to set and get the testNameSpacePrefix which is used to compute the
 effective key that will be used to persist a given object.
 
 Typically, this is used while unit testing the <code>PersistanceManager</code>
 so that the existing app's key wont be corrupted. Hence this is exposed in a private
 header.
 */
@property (nonatomic, strong) NSString *testNameSpacePrefix;

/*!
 Returns the effective key for a given key.
 
 The <code>PersistanceManager</code> computes the effective key by appending
 the given key to the <code>testNameSpacePrefix</code> specified.
 
 Eg: If the <code>testNameSpacePrefix</code> was specified as "Test", and the
 key is specified as "TestKey", then the effective key would be "Test.TestKey"
 
 @param key The key to which we are requesting an effective key.
 */
- (NSString *)effectiveKeyForKey:(NSString *)key;

@end

#endif /* PersistanceManager_Test_h */
