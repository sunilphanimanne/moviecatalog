//
//  PersistanceObjectProtocol.h
//  MovieCatalog
//
//  Created by Sunil Phani Manne on 16/01/19.
//  Copyright © 2019 Sunil Phani Manne. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PersistableObjectProtocol <NSObject>

/*!
 Retrieves the object that was persisted or a new instance of the Class conforming to this Protocol.
 
 @return Returns the object that was persisted or a new instance of the Class conforming to this Protocol.
 */
+ (instancetype)loadInstance;

/*!
 Persists the given object into the Persistance Store.
 
 */
- (void)persist;

@end
