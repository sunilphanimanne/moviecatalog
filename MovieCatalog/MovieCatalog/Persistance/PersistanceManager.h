//
//  PersistanceManager.h
//  MovieCatalog
//
//  Created by Sunil Phani Manne on 16/01/19.
//  Copyright © 2016 Sunil Phani Manne. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "PersistableObjectProtocol.h"

/*!
 PersistanceManager is used to persist and retrieve given objects which conforms to
 <code>PersistableObjectProtocol</code>.
 */
@interface PersistanceManager : NSObject

- (id) init __attribute__((unavailable("Must use sharedManager instead.")));

/*!
 Returns a singleton instance of <code>PersistanceManager</code>. If an instance of this
 class is needed, then this is ONLY way as the default <code>init</code> method
 is marked unavailable.
 
 @return Singleton instance of <code>PersistanceManager</code>
 */
+ (instancetype)sharedManager;

/*!
 Persists the given object of <code>PersistableObjectProtocol</code>.
 
 @param object Object that has to be persisted into the Persistance Store.
 @param key The object is persisted against this key.
 */
- (void)persistObject:(id <PersistableObjectProtocol>)object forKey:(NSString *)key;

/*!
 Retrieves the object that was persisted against the given key or nil if none was found.
 
 @param key Key to retrieve the object from the Persistance Store.
 @return Returns the object corresponding to the key provided. Returns nil if none was found.
 */
- (id)retrieveObjectForKey:(NSString *)key;

/*!
 Retrieves the object that was persisted against the given key or a new instance of the given class
 will be instantiated and returned.
 
 @param key Key to retrieve the object from the Persistance Store.
 @return Returns the object corresponding to the key provided. Returns a new instance if an existing object
 was not found.
 */
- (id)loadInstanceForClass:(Class)class andKey:(NSString *)key;

@end
