//
//  MovieListCell.h
//  MovieCatalog
//
//  Created by Sunil Phani Manne on 13/01/19.
//  Copyright © 2019 Sunil Phani Manne. All rights reserved.
//

#import <UIKit/UIKit.h>

/*!
 Table view cell to render the information about the Movie in the Movie List scene.
 */
@interface MovieListCell : UITableViewCell

/*!
 Label to render the title of the movie.
 */
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

/*!
 Image view to render the poster image of the view.
 */
@property (weak, nonatomic) IBOutlet UIImageView *posterImageView;

@end
