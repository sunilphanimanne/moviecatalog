//
//  ConversionUtils.h
//  MovieCatalog
//
//  Created by Sunil Phani Manne on 12/01/19.
//  Copyright © 2019 Sunil Phani Manne. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Awards.h"
#import "Person.h"
#import "Constants.h"

/*!
 Bunch of conversion utilities to process the string data into meaning information.
 
 Note: Signature of these methods are specified in C intentionally so as to
 ease the developer's life during the invocation.
 
 For eg:
 
 NSInteger anInteger = stringToInteger(@"24"); is much better than
 
 NSInteger anInteger = [ConversionUtils stringToInteger:@"24"];
 */
@interface ConversionUtils : NSObject

NSInteger stringToInteger(NSString *string);
float stringToFloat(NSString *string);
NSDate *stringToDate(NSString *string);
Person *stringToPerson(NSString *string);
NSArray <Person *> *stringToPersons(NSString *string);
Awards *stringToAwards(NSString *string);
MediaType stringToMediaType(NSString *string);
NSArray <NSString *> *stringToGenres(NSString *string);
NSURL *stringToURL(NSString *string);

@end
