//
//  ConversionUtils.m
//  MovieCatalog
//
//  Created by Sunil Phani Manne on 12/01/19.
//  Copyright © 2019 Sunil Phani Manne. All rights reserved.
//

#import "ConversionUtils.h"

@implementation ConversionUtils

NSInteger stringToInteger(NSString *string)
{
    return [string integerValue];
}

float stringToFloat(NSString *string)
{
    return [string floatValue];
}

NSDate *stringToDate(NSString *string)
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //Eg: 01 May 2015
    [dateFormatter setDateFormat:MovieDetailDateFormat];
    return [dateFormatter dateFromString:string];
}

Person *stringToPerson(NSString *string)
{
    Person *person = [[Person alloc] init];
    NSString *trimmedString = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    person.name = trimmedString;
    return person;
}

NSArray <Person *> *stringToPersons(NSString *string)
{
    //Eg: Robert Downey Jr., Chris Hemsworth, Mark Ruffalo, Chris Evans
    NSArray <NSString *> *names = [string componentsSeparatedByString:MovieDetailPersonDelimiter];
    NSMutableArray <Person *> *persons = [[NSMutableArray alloc] init];
    
    for (NSString *name in names) {
        Person *person = stringToPerson(name);
        [persons addObject:person];
    }
    return persons;
}

//Examples:
//1 nomination.
//3 wins & 14 nominations.
//"Awards": "7 wins & 45 nominations."
//Won 5 Oscars. Another 74 wins & 74 nominations.
//Won 2 Oscars. Another 152 wins & 155 nominations.
//Nominated for 1 Oscar. Another 38 wins & 79 nominations.
Awards *stringToAwards(NSString *string)
{
    //TODO: Implement this
    return nil;
}

MediaType stringToMediaType(NSString *string)
{
    return (MediaType)stringToInteger(string);
}

NSURL *stringToURL(NSString *string)
{
    NSURL *url = nil;
    
    if (string) {
        string = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        url = [NSURL URLWithString:string];
    }
    
    return url;
}

NSArray <NSString *> *stringToGenres(NSString *string)
{
    NSArray <NSString *> *genres = [string componentsSeparatedByString:MovieDetailGenreDelimiter];
    return genres;
}

@end
