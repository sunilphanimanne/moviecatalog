//
//  Utils.m
//  MovieCatalog
//
//  Created by Sunil Phani Manne on 11/01/19.
//  Copyright © 2019 Sunil Phani Manne. All rights reserved.
//

#import "Utils.h"

@implementation Utils

+ (NSDictionary *)jsonDictionaryFromData:(NSData *)data
                                   error:(NSError **)error
{
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data
                                                               options:NSJSONReadingMutableContainers
                                                                 error:error];
    return dictionary;
}

@end
