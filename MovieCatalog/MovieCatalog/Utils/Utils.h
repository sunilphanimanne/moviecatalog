//
//  Utils.h
//  MovieCatalog
//
//  Created by Sunil Phani Manne on 11/01/19.
//  Copyright © 2019 Sunil Phani Manne. All rights reserved.
//

#import <Foundation/Foundation.h>

/*!
 Utility class which provides a variety of utilties
 */
@interface Utils : NSObject

/*!
 Utility method to convert <code>NSData</code> of the JSON file into a
 <code>NSDictionary</code>
 
 @param data Binary data of the JSON file
 @param error The error object is populated by error which may occur during the
 conversion of the given data.
 */
+ (NSDictionary *)jsonDictionaryFromData:(NSData *)data
                                   error:(NSError **)error;

@end
