//
//  MovieList.h
//  MovieCatalog
//
//  Created by Sunil Phani Manne on 11/01/19.
//  Copyright © 2019 Sunil Phani Manne. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "PersistableObjectProtocol.h"

@class MovieDetail;

/*!
 This is a model object which contains the information
 corresponding to the list of movies to be rendered.
 */
@interface MovieList : NSObject <PersistableObjectProtocol>

/*!
 @brief Array of the <code>MovieDetail</code> contained inside the <code>MovieList</code>.
 */
@property (nonatomic, readonly) NSArray <MovieDetail *> *movieList;

/*!
 Convenience constructor to create a <code>MovieList</code> instance from a given
 JSON Dictionary
 
 @param dictionary Dictionary containing the JSON entries.
 @return Instance of <code>MovieList</code> constructued from the given JSON Dictionary.
 */
+ (instancetype)movieListFromDictionary:(NSDictionary *)dictionary;

/*!
 * Adds the given <code>MovieDetail</code> object to the existing list of movie detail objects.
 * @param movieDetail <code>MovieDetail</code> object.
 */
- (void)addMovieDetail:(MovieDetail *)movieDetail;

/*!
 * @brief Removes the given <code>MovieDetail</code> object from the existing list of movie detail objects.
 * @param movieDetail MovieDetail object.
 */
- (void)removeMovieDetail:(MovieDetail *)movieDetail;

@end
