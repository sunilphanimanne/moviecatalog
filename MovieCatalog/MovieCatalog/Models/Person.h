//
//  Person.h
//  MovieCatalog
//
//  Created by Sunil Phani Manne on 11/01/19.
//  Copyright © 2019 Sunil Phani Manne. All rights reserved.
//

#import <Foundation/Foundation.h>

/*!
 Model object containing the information about the people.
 
 For example: Directors of the movie, Writers of the movie etc.
 */
@interface Person : NSObject

/*!
 Holds the name of the person.
 */
@property (nonatomic, strong) NSString *name;

/*!
 Holds the contribution of the person. For example writer contributing to a part of the movie.
 */
@property (nonatomic, strong) NSString *contribution;

@end
