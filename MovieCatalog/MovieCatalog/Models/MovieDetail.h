//
//  MovieDetail.h
//  MovieCatalog
//
//  Created by Sunil Phani Manne on 11/01/19.
//  Copyright © 2019 Sunil Phani Manne. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "Person.h"
#import "Awards.h"
#import "Constants.h"
#import "PersistableObjectProtocol.h"

/*
 Important Note:
 This is an optimisation switch which can be turned ON and OFF by specifying 1 or 0.
 
 When PROCESS_VALUES is 0, then no processing of information is done.
 When PROCESS_VALUES is 1, then processing of information is done and the corresponding
 information is available to be queried through <code>getProcessedValueForKey:</code>
 method.
 
 The key corresponding to the value will be the key returned in the JSON file.
 Refer to the Constants.m for the constants corresponding to the keys.
*/
#define PROCESS_VALUES 0

/*!
 <code>MovieDetail</code> is the model object that contains the information
 about the details of a movie.
 */
@interface MovieDetail : NSObject <PersistableObjectProtocol>

/*!
 Holds the title of the movie.
 */
@property (nonatomic, copy) NSString *title;

/*!
 Holds the year of the movie.
 
 Processed Value: NSInteger year;
 */
@property (nonatomic, copy) NSString *year;

/*!
 Holds the rating of the movie.
 */
@property (nonatomic, copy) NSString *rating;

/*!
 Holds the release date of the movie.
 
 Processed Value: NSDate *releaseDate;
 */
@property (nonatomic, copy) NSString *releaseDate;

/*!
 Holds the run time of the movie in minutes.
 
 Processed Value: NSInteger runTimeInMinutes;
 */
@property (nonatomic, copy) NSString *runTimeInMinutes;

/*!
 Holds the genre(s) of the movie.
 
 Processed Value: NSArray <NSString *> *genres;
 */
@property (nonatomic, copy) NSString *genres;

/*!
 Holds the director(s) of the movie.
 
 Processed Value: NSArray <Person *> *directors;
 */
@property (nonatomic, copy) NSString *directors;

/*!
 Holds the writer(s) of the movie.
 
 Processed Value: NSArray <Person *> *writers;
 */
@property (nonatomic, copy) NSString *writers;

/*!
 Holds the actor(s) of the movie.
 
 Processed Value: NSArray <Person *> *actors;
 */
@property (nonatomic, copy) NSString *actors;

/*!
 Holds the plot of the movie.
 */
@property (nonatomic, copy) NSString *plot;

/*!
 Holds the language(s) of the movie.
 
 Processed Value: NSArray <NSString *> *language;
 */
@property (nonatomic, copy) NSString *language;

/*!
 Holds the country of the movie.
 */
@property (nonatomic, copy) NSString *country;

/*!
 Hold the award(s) of the movie.
 
 Processed Value: Awards *awards;
 */
@property (nonatomic, copy) NSString *awards;

/*!
 Holds the URL of the poster image of the movie.
 */
@property (nonatomic, strong) NSURL *posterURL;

/*!
 Holds the metascore of the movie.
 
 Processed Value: float metaScore;
 */
@property (nonatomic, copy) NSString *metaScore;

/*!
 Holds the IMDB Rating of the movie.
 
 Processed Value: float imdbRating;
 */
@property (nonatomic, copy) NSString *imdbRating;

/*!
 Holds the IMDB Votes of the movie.
 
 Processed Value: NSInteger imdbVotes;
 */
@property (nonatomic, copy) NSString *imdbVotes;

/*!
 Holds the IMDBID of the movie.
 */
@property (nonatomic, copy) NSString *imdbID;

/*!
 Holds the type of the movie.
 
 Processed Value: MediaType type;
 */
@property (nonatomic, copy) NSString *type;

/*!
 Holds the DVD info of the movie.
 */
@property (nonatomic, copy) NSString *dvd;

/*!
 Holds the box office info of the movie.
 */
@property (nonatomic, copy) NSString *boxOffice;

/*!
 Holds the production info of the movie.
 */
@property (nonatomic, copy) NSString *production;

/*!
 Holds the website address of the movie.
 */
@property (nonatomic, copy) NSString *website;

/*!
 Holds the response of the movie.
 */
@property (nonatomic, copy) NSString *response;

/*!
 Convenience constructor to create a <code>MovieDetail</code> instance from a given
 JSON Dictionary

 @param dictionary Dictionary containig the JSON entries.
 @return Instance of <code>MovieDetail</code> constructued from the given JSON Dictionary.
 */
+ (instancetype)movieDetailFromDictionary:(NSDictionary *)dictionary;

/*!
 Returns the processed value for a given key. Please refer to the Processed value of
 the corresponding key in the property's comment.
 
 For example:
 
 [movieDetail getProcessedValueForKey:@"Writer"];
 
 would return an instance <code>NSArray <Person *> *writers</code>.
 
 This processed info is very useful in usecases where in you would want to make some selection
 
 based on the writers.
 
 @param key Key for the JSON attribute.
 @return Returns the processed value for a given key.
 */
- (id)getProcessedValueForKey:(NSString *)key;

@end
