//
//  Awards
//  MovieCatalog
//
//  Created by Sunil Phani Manne on 11/01/19.
//  Copyright © 2019 Sunil Phani Manne. All rights reserved.
//

#import <Foundation/Foundation.h>

/*!
 Model object containing the Awards that a movie has been nominated to and received.
 */
@interface Awards : NSObject

/*!
 Holds the number of Oscar nominations of the movie.
 */
@property (nonatomic, assign) NSInteger oscarNominations;

/*!
 Holds the number of Oscar wins of the movie
 */
@property (nonatomic, assign) NSInteger oscarWins;

/*!
 Holds the number of other nominations of the movie.
 */
@property (nonatomic, assign) NSInteger otherNominations;

/*!
 Holds the number of the other wins of the movie.
 */
@property (nonatomic, assign) NSInteger otherWins;

@end
