//
//  MovieDetail.m
//  MovieCatalog
//
//  Created by Sunil Phani Manne on 11/01/19.
//  Copyright © 2019 Sunil Phani Manne. All rights reserved.
//

#import "MovieDetail.h"

#import "Constants.h"
#import "ConversionUtils.h"
#import "PersistanceManager.h"

NSString *const MovieDetailPersistanceKey = @"Persistence.MovieDetail";

@interface MovieDetail () <NSSecureCoding>

@property (nonatomic, strong) NSMutableDictionary *processedFields;

@end

@implementation MovieDetail

- (instancetype)init
{
    self = [super init];
    _processedFields = [[NSMutableDictionary alloc] initWithCapacity:MovieDetailProcessedFieldsInitialCapacity];
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [self init]) {
        _title = [aDecoder decodeObjectForKey:MovieDetailTitleKey];
        _year = [aDecoder decodeObjectForKey:MovieDetailYearKey];
        _rating = [aDecoder decodeObjectForKey:MovieDetailRatingsKey];
        _releaseDate = [aDecoder decodeObjectForKey:MovieDetailReleasedKey];
        _runTimeInMinutes = [aDecoder decodeObjectForKey:MovieDetailRuntimeKey];
        _genres = [aDecoder decodeObjectForKey:MovieDetailGenreKey];
        _directors = [aDecoder decodeObjectForKey:MovieDetailDirectorKey];
        _writers = [aDecoder decodeObjectForKey:MovieDetailWriterKey];
        _actors = [aDecoder decodeObjectForKey:MovieDetailActorsKey];
        _plot = [aDecoder decodeObjectForKey:MovieDetailPlotKey];
        _language = [aDecoder decodeObjectForKey:MovieDetailLanguageKey];
        _country = [aDecoder decodeObjectForKey:MovieDetailCountryKey];
        _awards = [aDecoder decodeObjectForKey:MovieDetailAwardsKey];
        _posterURL = [aDecoder decodeObjectForKey:MovieDetailPosterKey];
        _metaScore = [aDecoder decodeObjectForKey:MovieDetailMetaScoreKey];
        _imdbVotes = [aDecoder decodeObjectForKey:MovieDetailImdbVotesKey];
        _imdbRating = [aDecoder decodeObjectForKey:MovieDetailImdbRatingKey];
        _imdbID = [aDecoder decodeObjectForKey:MovieDetailImdbIDKey];
        _type = [aDecoder decodeObjectForKey:MovieDetailTypeKey];
        _dvd = [aDecoder decodeObjectForKey:MovieDetailDVDKey];
        _boxOffice = [aDecoder decodeObjectForKey:MovieDetailBoxOfficeKey];
        _production = [aDecoder decodeObjectForKey:MovieDetailProductionKey];
        _website = [aDecoder decodeObjectForKey:MovieDetailWebsiteKey];
        _response = [aDecoder decodeObjectForKey:MovieDetailResponseKey];
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:_title forKey:MovieDetailTitleKey];
    [aCoder encodeObject:_year forKey:MovieDetailYearKey];
    [aCoder encodeObject:_rating forKey:MovieDetailRatingsKey];
    [aCoder encodeObject:_releaseDate forKey:MovieDetailReleasedKey];
    [aCoder encodeObject:_runTimeInMinutes forKey:MovieDetailRuntimeKey];
    [aCoder encodeObject:_directors forKey:MovieDetailDirectorKey];
    [aCoder encodeObject:_writers forKey:MovieDetailWriterKey];
    [aCoder encodeObject:_actors forKey:MovieDetailActorsKey];
    [aCoder encodeObject:_plot forKey:MovieDetailPlotKey];
    [aCoder encodeObject:_language forKey:MovieDetailLanguageKey];
    [aCoder encodeObject:_country forKey:MovieDetailCountryKey];
    [aCoder encodeObject:_awards forKey:MovieDetailAwardsKey];
    [aCoder encodeObject:_posterURL forKey:MovieDetailPosterKey];
    [aCoder encodeObject:_metaScore forKey:MovieDetailMetaScoreKey];
    [aCoder encodeObject:_imdbVotes forKey:MovieDetailImdbVotesKey];
    [aCoder encodeObject:_imdbRating forKey:MovieDetailImdbRatingKey];
    [aCoder encodeObject:_imdbID forKey:MovieDetailImdbIDKey];
    [aCoder encodeObject:_type forKey:MovieDetailTypeKey];
    [aCoder encodeObject:_dvd forKey:MovieDetailDVDKey];
    [aCoder encodeObject:_boxOffice forKey:MovieDetailBoxOfficeKey];
    [aCoder encodeObject:_production forKey:MovieDetailProductionKey];
    [aCoder encodeObject:_website forKey:MovieDetailWebsiteKey];
    [aCoder encodeObject:_response forKey:MovieDetailResponseKey];
}

+ (instancetype)loadInstance
{
    return [[PersistanceManager sharedManager] loadInstanceForClass:self
                                                             andKey:MovieDetailPersistanceKey];
}

- (void)persist
{
    [[PersistanceManager sharedManager] persistObject:self
                                               forKey:MovieDetailPersistanceKey];
}

//Setters for the processing-needed properties...
- (void)setYear:(NSString *)string
{
    if(![_year isEqualToString:string]) {
        _year = [string copy];
        
#if PROCESS_VALUES
        self.processedFields[MovieDetailYearKey] = @(stringToInteger(string));
#endif
    }
}

- (void)setReleaseDate:(NSString *)string
{
    if(![_releaseDate isEqualToString:string]) {
        _releaseDate = [string copy];
        
#if PROCESS_VALUES
        self.processedFields[MovieDetailReleasedKey] = stringToDate(string);
#endif
    }
}

- (void)setRunTimeInMinutes:(NSString *)string
{
    if(![_runTimeInMinutes isEqualToString:string]) {
        _runTimeInMinutes = [string copy];
        
#if PROCESS_VALUES
        self.processedFields[MovieDetailRuntimeKey] = @(stringToInteger(string));
#endif
    }
}

- (void)setGenres:(NSString *)string
{
    if(![_genres isEqualToString:string]) {
        _genres = [string copy];
        
#if PROCESS_VALUES
        self.processedFields[MovieDetailGenreKey] = stringToGenres(string);
#endif
    }
}

- (void)setDirectors:(NSString *)string
{
    if(![_directors isEqualToString:string]) {
        _directors = [string copy];
        
#if PROCESS_VALUES
        self.processedFields[MovieDetailDirectorKey] = stringToPersons(string);
#endif
    }
}

- (void)setWriters:(NSString *)string
{
    if(![_writers isEqualToString:string]) {
        _writers = [string copy];
        
#if PROCESS_VALUES
        self.processedFields[MovieDetailWriterKey] = stringToPersons(string);
#endif
    }
}

- (void)setActors:(NSString *)string
{
    if(![_actors isEqualToString:string]) {
        _actors = [string copy];
        
#if PROCESS_VALUES
        self.processedFields[MovieDetailActorsKey] = stringToPersons(string);
#endif
    }
}

- (void)setAwards:(NSString *)string
{
    if(![_awards isEqualToString:string]) {
        _awards = [string copy];
        
#if PROCESS_VALUES
        self.processedFields[MovieDetailAwardsKey] = stringToPersons(string);
#endif
    }
}

- (void)setMetaScore:(NSString *)string
{
    if(![_metaScore isEqualToString:string]) {
        _metaScore = [string copy];
        
#if PROCESS_VALUES
        self.processedFields[MovieDetailMetaScoreKey] = stringToPersons(string);
#endif
    }
}

- (void)setImdbRating:(NSString *)string
{
    if(![_imdbRating isEqualToString:string]) {
        _imdbRating = [string copy];
        
#if PROCESS_VALUES
        self.processedFields[MovieDetailImdbRatingKey] = @(stringToInteger(string));
#endif
    }
}

- (void)setImdbVotes:(NSString *)string
{
    if(![_imdbVotes isEqualToString:string]) {
        _imdbVotes = [string copy];
        
#if PROCESS_VALUES
        self.processedFields[MovieDetailImdbVotesKey] = @(stringToInteger(string));
#endif
    }
}

- (void)setType:(NSString *)string
{
    if(![_type isEqualToString:string]) {
        _type = [string copy];
        
#if PROCESS_VALUES
        self.processedFields[MovieDetailTypeKey] = @(stringToMediaType(string));
#endif
    }
}

- (id)getProcessedValueForKey:(NSString *)key
{
    id returnObject = nil;
    
    if (key && PROCESS_VALUES) {
        returnObject = self.processedFields[key];
    }
    
    return returnObject;
}

+ (MovieDetail *)movieDetailFromDictionary:(NSDictionary *)dictionary
{
    MovieDetail *movieDetail = [[MovieDetail alloc] init];
    
    movieDetail.title = dictionary[MovieDetailTitleKey];
    movieDetail.year = dictionary[MovieDetailYearKey];
    movieDetail.rating = dictionary[MovieDetailRatedKey];
    movieDetail.releaseDate = dictionary[MovieDetailReleasedKey];
    movieDetail.runTimeInMinutes = dictionary[MovieDetailRuntimeKey];
    movieDetail.genres = dictionary[MovieDetailGenreKey];
    movieDetail.directors = dictionary[MovieDetailDirectorKey];
    movieDetail.writers = dictionary[MovieDetailWriterKey];
    movieDetail.actors = dictionary[MovieDetailActorsKey];
    movieDetail.plot = dictionary[MovieDetailPlotKey];
    movieDetail.language = dictionary[MovieDetailLanguageKey];
    movieDetail.country = dictionary[MovieDetailCountryKey];
    movieDetail.awards = dictionary[MovieDetailAwardsKey];
    movieDetail.posterURL = stringToURL(dictionary[MovieDetailPosterKey]);
    if (!movieDetail.posterURL) {
        //To handle the special case of "The Avengers" movie poster
        movieDetail.posterURL = stringToURL(dictionary[[MovieDetailPosterKey stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]]);
    }
    movieDetail.metaScore = dictionary[MovieDetailMetaScoreKey];
    movieDetail.imdbRating = dictionary[MovieDetailImdbRatingKey];
    movieDetail.imdbVotes = dictionary[MovieDetailImdbVotesKey];
    movieDetail.imdbID = dictionary[MovieDetailImdbIDKey];
    movieDetail.type = dictionary[MovieDetailTypeKey];
    movieDetail.dvd = dictionary[MovieDetailDVDKey];
    movieDetail.boxOffice = dictionary[MovieDetailBoxOfficeKey];
    movieDetail.production = dictionary[MovieDetailProductionKey];
    movieDetail.website = dictionary[MovieDetailWebsiteKey];
    movieDetail.response = dictionary[MovieDetailResponseKey];
    
    return movieDetail;
}

+ (BOOL)supportsSecureCoding
{
    return YES;
}

@end
