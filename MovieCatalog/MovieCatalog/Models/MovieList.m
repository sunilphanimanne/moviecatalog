//
//  MovieList.m
//  MovieCatalog
//
//  Created by Sunil Phani Manne on 11/01/19.
//  Copyright © 2019 Sunil Phani Manne. All rights reserved.
//

#import "MovieList.h"

#import "MovieDetail.h"
#import "PersistanceManager.h"

NSString *const MovieListPersistanceKey = @"Persistence.MovieList";

@interface MovieList () <NSSecureCoding>

@property (nonatomic, strong) NSMutableArray <MovieDetail *> *movieList;

@end

@implementation MovieList

- (instancetype)init
{
    self = [super init];
    
    _movieList = [[NSMutableArray alloc] initWithCapacity:10];
    
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [self init]) {
        _movieList = [aDecoder decodeObjectForKey:MovieListMovieListKey];
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:_movieList forKey:MovieListMovieListKey];
}

+ (instancetype)loadInstance
{
    return [[PersistanceManager sharedManager] loadInstanceForClass:self
                                                             andKey:MovieListPersistanceKey];
}

- (void)persist
{
    [[PersistanceManager sharedManager] persistObject:self
                                               forKey:MovieListPersistanceKey];
}


+ (MovieList *)movieListFromDictionary:(NSDictionary *)dictionary
{
    MovieList *movieList = nil;
    
    if (dictionary && [dictionary count]) {
        movieList = [[MovieList alloc] init];
        NSArray *movies = dictionary[MovieListMoviesKey];
        
        for (NSDictionary *movie in movies) {
            MovieDetail *movieDetail = [MovieDetail movieDetailFromDictionary:movie];
            [movieList addMovieDetail:movieDetail];
        }
    }
    
    return movieList;
}

- (void)addMovieDetail:(MovieDetail *)movieDetail
{
    if (movieDetail) {
        [_movieList addObject:movieDetail];
    }
}

- (void)removeMovieDetail:(MovieDetail *)movieDetail
{
    if (movieDetail) {
        [_movieList removeObject:movieDetail];
    }
}

- (NSArray *)movieList
{
    return [_movieList copy];
}

+ (BOOL)supportsSecureCoding
{
    return YES;
}

@end
