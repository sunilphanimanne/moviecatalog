//
//  DataDownloader.m
//  MovieCatalog
//
//  Created by Sunil Phani Manne on 11/01/19.
//  Copyright © 2019 Sunil Phani Manne. All rights reserved.
//

#import "DataDownloader.h"

#import "Utils.h"
#import "Constants.h"

@implementation DataDownloader

+ (void)downloadJSONDataFromURL:(NSURL *)url
              completionHandler:(JSONDataDownloadCompletionHandler)completionHandler
{
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDownloadTask *downloadTask =
            [session downloadTaskWithRequest:request
                           completionHandler:
                  ^(NSURL *location, NSURLResponse *response, NSError *error) {
                      if(completionHandler) {
                          NSDictionary *jsonDictionary = nil;
                          if (!error) {
                              NSData *data = [NSData dataWithContentsOfURL:location];
                              
                              //convert the data into dictionary
                              jsonDictionary = [Utils jsonDictionaryFromData:data
                                                                       error:&error];
                          }
                          completionHandler(jsonDictionary, error);
                      }else {
                          NSLog(@"***Warning: DataDownloader.downloadJSONDataFromURL:: No completionHandler!***");
                      }
                  }];
    
    [downloadTask resume];
}

@end
