//
//  DataDownloader.h
//  MovieCatalog
//
//  Created by Sunil Phani Manne on 11/01/19.
//  Copyright © 2019 Sunil Phani Manne. All rights reserved.
//

#import <Foundation/Foundation.h>

/*!
 Completion handler invoked after the download and conversion of the JSON file
 into <code>NSDictionary</code>.
 */
typedef void (^JSONDataDownloadCompletionHandler)(NSDictionary *jsonDictionary, NSError *error);

/*!
 Downloader utility to download a given JSON file from local or remote URL  and convert
 the entries of the JSON into Keys/values into a Dictionary
 */
@interface DataDownloader : NSObject

/*!
 Downloads the JSON file from a given local or remote URL and converts
 the entries of the JSON into Keys/values into a Dictionary.
 
 @param url Remote or local URL of the JSON file.
 @param completionHandler Completion handler that would be called after finishing the download and conversion.
 */
+ (void)downloadJSONDataFromURL:(NSURL *)url
              completionHandler:(JSONDataDownloadCompletionHandler)completionHandler;

@end
