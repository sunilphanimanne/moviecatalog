//
//  MovieListViewController.m
//  MovieCatalog
//
//  Created by Sunil Phani Manne on 11/01/19.
//  Copyright © 2019 Sunil Phani Manne. All rights reserved.
//

#import "MovieListViewController.h"
#import "MovieDetailViewController.h"

#import "Utils.h"
#import "Constants.h"
#import "DataDownloader.h"
#import "MovieListCell.h"

#define LOAD_AND_STORE_FROM_PRESISTANCE_STORE 1

typedef void (^DataLoadCompletionHandler)(id dataObject);

@interface MovieListViewController ()

@property MovieList *movieListObject;
@property (nonatomic, readwrite) NSMutableDictionary *posterImageCache;

@end

@implementation MovieListViewController

//Queue used to persist the data
dispatch_queue_t _persistanceSerialQueue;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _posterImageCache = [[NSMutableDictionary alloc] initWithCapacity:MovieListPosterImageCacheInitialCapacity];
    _persistanceSerialQueue = dispatch_queue_create(PersistanceSerialQueueName,
                                                    DISPATCH_QUEUE_SERIAL);
    
    [self registerCellFromNib];
    
    [self startDownloadDataTask];
    
    self.detailViewController = (MovieDetailViewController *)[[self.splitViewController.viewControllers lastObject] topViewController];
}

- (void)registerCellFromNib
{
    UINib *movieListCellNib = [UINib nibWithNibName:MovieListCellNibName
                                               bundle:nil];
    [self.tableView registerNib:movieListCellNib
         forCellReuseIdentifier:MovieListCellReuseIdentifier];
}

- (void)startDownloadDataTask
{
    //Pre handler
    [self preDataDownloadHandler];
    
    //Actual handler
    [self dataDownloadHandler:^(NSDictionary *jsonDictionary, NSError *error) {
        if (!error) {
            //Post handler
            [self postDataDownloadHandler:jsonDictionary
                                    error:error];
        }else {
            NSLog(MovieListJSONDownloadError, error.description);
        }
    }];
}

- (void)dataDownloadHandler:(JSONDataDownloadCompletionHandler)completionHandler
{
    NSURL *movieListURL = [NSURL URLWithString:MovieListJSONDownloadURL];
    [DataDownloader downloadJSONDataFromURL:movieListURL
                          completionHandler:^(NSDictionary *jsonDictionary, NSError *error) {
                              if (completionHandler) {
                                  completionHandler(jsonDictionary, error);
                              }else {
                                  NSLog(@"***Warning: MovieListViewController.dataDownloadHandler: No completionHandler!***");
                              }
                          }];
    
}

- (void)preDataDownloadHandler
{
#if LOAD_AND_STORE_FROM_PRESISTANCE_STORE
    
    [self loadPersistedData:^(id dataObject) {
        /*
         Important Note:
         Lock the movieList model object so as to avoid any threading clashes
         bcoz there are two things that can happen in parallel -
         (1) the asynchronous loadPersistedData method
         (2) the actual network call which would fetch the JSON...
         */
        
        //Enter into the CRITICAL section ONLY if you have a non-null dataObject fetched from the Persistent store!
        if (dataObject) {
            @synchronized (_movieListObject) {
                _movieListObject = dataObject;
            }
            
            NSLog(@"Data retrieved from Persistant Store, Refreshing the tableView!");
            
            //Refresh the TableView with the Model Object...
            [self refreshTable];
        }
    }];
#endif
}

- (void)postDataDownloadHandler:(NSDictionary *)jsonDictionary error:(NSError *)error
{
    if (error) {
         NSLog(MovieListJSONParsingError, error.description);
    }else {
        if (jsonDictionary) {
            //Process everything and have it ready before entering the CRITICAL section.
            MovieList *tempMovieList = [MovieList movieListFromDictionary:jsonDictionary];
            
            /*
             Important Note:
             Lock the movieList model object so as to avoid any threading clashes
             bcoz there are two things that can happen in parallel -
             (1) the asynchronous loadPersistedData method
             (2) the actual network call which would fetch the JSON...
             */
            @synchronized(_movieListObject) {
                _movieListObject = tempMovieList;
            }
            
            NSLog(@"Data retrieved from JSON Service, Refreshing the tableView!");
            
            //Refresh the TableView with the Model Object...
            [self refreshTable];
            
            NSLog(@"Persisting the data into the Persistant Store!");
            [self persistModelData];
        }
    }
}

- (void)refreshTable
{
    //Reload the table with the fresh data
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
    
    //Select the first item if the split view is being shown...
    if ([self isSplitViewBeingShown]) {
        [self selectFirstItemInTableView];
    }
}

- (BOOL)isSplitViewBeingShown
{
    return ([self.splitViewController.viewControllers count] == 2);
}

- (void)selectFirstItemInTableView {
    //Schedule a method on *MAIN THREAD* to select the first row item once the reloadData is finished...
    dispatch_async(dispatch_get_main_queue(), ^{
        //If the table is loaded with at least one item... then select the first row...
        if ([self.tableView numberOfRowsInSection:0]) {
            NSIndexPath *firstItemIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
            [self.tableView selectRowAtIndexPath:firstItemIndexPath
                                        animated:YES
                                  scrollPosition:UITableViewScrollPositionTop];
            
            [self showDetailForRowAtIndexPath:firstItemIndexPath];
        }
    });
}


- (void)persistModelData
{
#if LOAD_AND_STORE_FROM_PRESISTANCE_STORE
    //Persist the MovieList Model Object on a *SECONDARY* thread!
    dispatch_async(_persistanceSerialQueue, ^{
        [_movieListObject persist];
    });
#endif
}

- (void)loadPersistedData:(DataLoadCompletionHandler)completionHandler
{
    __block MovieList *movieList = nil;
    
    dispatch_async(_persistanceSerialQueue, ^{
        movieList = [MovieList loadInstance];
        
        if (completionHandler) {
            completionHandler(movieList);
        }
    });
}

- (void)viewWillAppear:(BOOL)animated
{
    self.clearsSelectionOnViewWillAppear = self.splitViewController.isCollapsed;
    [super viewWillAppear:animated];
}

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:MovieDetailSegueIdentifier]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        MovieDetail *detailObject = self.movieListObject.movieList[indexPath.row];
        MovieDetailViewController *controller = (MovieDetailViewController *)[[segue destinationViewController] topViewController];
        [controller setDetailItem:detailObject];
        controller.posterImage = _posterImageCache[indexPath];
        controller.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;
        controller.navigationItem.leftItemsSupplementBackButton = YES;
    }
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return MovieListTableViewSectionCount;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    return MovieListTableViewCellHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.movieListObject.movieList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MovieListCell *cell = (MovieListCell *)[tableView dequeueReusableCellWithIdentifier:MovieListCellReuseIdentifier
                                                            forIndexPath:indexPath];
        
    MovieDetail *movieDetail = self.movieListObject.movieList[indexPath.row];
    
    [self loadTheCell:cell atIndexPath:indexPath
             withData:movieDetail];
    
    return cell;
}

- (void)loadTheCell:(MovieListCell *)cell
        atIndexPath:(NSIndexPath *)indexPath
           withData:(MovieDetail *)data
{
    cell.titleLabel.text = data.title;
    [self loadTheCell:cell
          atIndexPath:indexPath withPosterImageAtURL:data.posterURL];
    
    [self configureCell:cell];
}

- (void)configureCell:(MovieListCell *)cell
{
    [self configureTheBackgroundViewForCell:cell];
}

- (void)configureTheBackgroundViewForCell:(MovieListCell *)cell
{
    if (cell.selectedBackgroundView.tag != BackgroundViewTag) {
        UIView *customColorView = [[UIView alloc] init];
        customColorView.tag = BackgroundViewTag;
        customColorView.backgroundColor = MOVIE_DETAIL_CELL_SELECTION_COlOR;
        cell.selectedBackgroundView = customColorView;
    }
}

- (void)loadTheCell:(MovieListCell *)cell
        atIndexPath:(NSIndexPath *)indexPath
         withPosterImageAtURL:(NSURL *)posterURL
{
    cell.posterImageView.image = DEFAULT_MOVIE_POSTER_IMAGE;
    _posterImageCache[indexPath] = cell.posterImageView.image;
    __block UIImage *image = nil;
    
    if (!image) {
        if (posterURL) {
            NSURLSession *session = [NSURLSession sharedSession];
            NSURLSessionTask *task = [session dataTaskWithURL:posterURL completionHandler:^(NSData * data, NSURLResponse *response, NSError *error) {
                if (data) {
                    image = [UIImage imageWithData:data];
                    if (image) {
                        //update the cache...
                        _posterImageCache[indexPath] = image;
                        
                        //... and update the cell!
                        dispatch_async(dispatch_get_main_queue(), ^{
                            MovieListCell *latestCell = (id)[self.tableView cellForRowAtIndexPath:indexPath];
                            if (latestCell) {
                                latestCell.posterImageView.image = image;
                            }
                        });
                    }
                }
            }];
            
            [task resume];
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self showDetailForRowAtIndexPath:indexPath];
}

- (void)showDetailForRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:MovieDetailSegueIdentifier
                              sender:self];
}

@end
