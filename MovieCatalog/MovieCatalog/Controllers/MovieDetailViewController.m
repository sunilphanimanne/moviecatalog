//
//  MovieDetailViewController.m
//  MovieCatalog
//
//  Created by Sunil Phani Manne on 11/01/19.
//  Copyright © 2019 Sunil Phani Manne. All rights reserved.
//

#import "MovieDetailViewController.h"

@implementation MovieDetailViewController

- (void)configureView {
    // Update the user interface for the detail item.
    if (self.detailItem) {
        self.titleLabel.text = self.detailItem.title;
        if (self.posterImage) {
           self.posterImageView.image = self.posterImage;
        }else {
            self.posterImageView.image = DEFAULT_MOVIE_POSTER_IMAGE;
        }
        
        self.yearLabel.text = self.detailItem.year;
        self.ratedLabel.text = self.detailItem.rating;
        self.releasedLabel.text = self.detailItem.releaseDate;
        self.runtimeLabel.text = self.detailItem.runTimeInMinutes;
        self.genreLabel.text = self.detailItem.genres;
        self.languageLabel.text = self.detailItem.language;
        self.writersLabel.text = self.detailItem.writers;
        self.directorsLabel.text = self.detailItem.directors;
        self.actorsLabel.text = self.detailItem.actors;
        self.plotTextView.text = self.detailItem.plot;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureView];
}

#pragma mark - Managing the detail item

- (void)setDetailItem:(MovieDetail *)newDetailItem {
    if (![_detailItem isEqual:newDetailItem]) {
        _detailItem = newDetailItem;
        
        // Update the view.
        [self configureView];
    }
}


@end
