//
//  MovieListViewController.h
//  MovieCatalog
//
//  Created by Sunil Phani Manne on 11/01/19.
//  Copyright © 2019 Sunil Phani Manne. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MovieList.h"

@class MovieDetailViewController;

/*!
 This is the main controller which renders the list of movies.
 */
@interface MovieListViewController : UITableViewController

/*!
 Sets and gets the model object <code>MovieList</code>. This contains the information
 corresponding to the list of movies to be rendered.
 */
@property (nonatomic, strong) MovieList *movieList;

/*!
 This is the detail view controller which renders the details of the movie selected in the
 <code>MovieListViewController</code>
 */
@property (nonatomic, strong) MovieDetailViewController *detailViewController;

/*!
 This cache contains the images that were already fetched for displaying the list of movies.
 
 @return Returns the dictionary containing the poster image for the given NSIndexPath as a key.
 */
@property (nonatomic, strong, readonly) NSDictionary *posterImageCache;

@end

