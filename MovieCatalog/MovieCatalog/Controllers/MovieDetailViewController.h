//
//  MovieDetailViewController.h
//  MovieCatalog
//
//  Created by Sunil Phani Manne on 11/01/19.
//  Copyright © 2019 Sunil Phani Manne. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MovieDetail.h"

/*!
 @brief Detail view controller which renders the details of the movie selected in the
 <code>MovieListViewController</code>
 */
@interface MovieDetailViewController : UIViewController

/*!
 Image of the Poster of a movie that needs to be rendered.
 */
@property (strong, nonatomic) UIImage *posterImage;

/*!
 <code>MovieDetail</code> is the model object that contains the information
 about the details of the movie that needs to be rendered.
 */
@property (strong, nonatomic) MovieDetail *detailItem;

/*!
 Label that renders the title of the movie.
 */
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

/*!
 Image view that renders the <code>posterImage</code>.
 */
@property (weak, nonatomic) IBOutlet UIImageView *posterImageView;

/*!
 Label that renders the year of the movie.
 */
@property (weak, nonatomic) IBOutlet UILabel *yearLabel;

/*!
 Label that renders the rating of the movie.
 */
@property (weak, nonatomic) IBOutlet UILabel *ratedLabel;

/*!
 Label that renders the released date of the movie.
 */
@property (weak, nonatomic) IBOutlet UILabel *releasedLabel;

/*!
 Label that renders the run time of the movie in minutes.
 */
@property (weak, nonatomic) IBOutlet UILabel *runtimeLabel;

/*!
 Label that renders the genre of the movie.
 */
@property (weak, nonatomic) IBOutlet UILabel *genreLabel;

/*!
 Label that renders the languages of the movie.
 */
@property (weak, nonatomic) IBOutlet UILabel *languageLabel;

/*!
 Label that renders the writer(s) of the movie.
 */
@property (weak, nonatomic) IBOutlet UILabel *writersLabel;

/*!
 Label that renders the director(s) of the movie.
 */
@property (weak, nonatomic) IBOutlet UILabel *directorsLabel;

/*!
 Label that renders the actor(s) of the movie.
 */
@property (weak, nonatomic) IBOutlet UILabel *actorsLabel;

/*!
 Label that renders the plot of the movie.
 */
@property (weak, nonatomic) IBOutlet UITextView *plotTextView;

@end

