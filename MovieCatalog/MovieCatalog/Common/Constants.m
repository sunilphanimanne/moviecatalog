//
//  Constants.m
//  MovieCatalog
//
//  Created by Sunil Phani Manne on 11/01/19.
//  Copyright © 2019 Sunil Phani Manne. All rights reserved.
//

#import "Constants.h"

//Persistance Manager Constants
NSString *const PersistanceManagerTestPrefixSeparator = @".";

//Miscellaneous
NSString *const MovieDetailGenreDelimiter = @", ";
NSString *const MovieDetailPersonDelimiter = @", ";
NSString *const MovieDetailDateFormat = @"d MMM yyyy";
NSString *const MovieDetailSegueIdentifier = @"showDetail";
NSString *const MovieListCellNibName = @"MovieListCell";
NSString *const MovieListCellReuseIdentifier = @"MovieListCell";

//JSON Service URL
NSString *const MovieListJSONDownloadURL = @"https://api.myjson.com/bins/18buhu";

//JSON Parsing Errors
NSString *const MovieListJSONParsingError = @"***Error: Error while parsing the JSON data. Cause:%@";
NSString *const MovieListJSONDownloadError = @"***Error: Error while downloading MovieList JSON. Cause:%@";

//MovieList Model Object Keys
NSString *const MovieListMovieListKey = @"MovieList";
NSString *const MovieListMoviesKey = @"movies";

//MovieDetail Model Object Keys
NSString *const MovieDetailTitleKey = @"Title";
NSString *const MovieDetailYearKey = @"Year";
NSString *const MovieDetailRatedKey = @"Rated";
NSString *const MovieDetailReleasedKey = @"Released";
NSString *const MovieDetailRuntimeKey = @"Runtime";
NSString *const MovieDetailGenreKey = @"Genre";
NSString *const MovieDetailDirectorKey = @"Director";
NSString *const MovieDetailWriterKey = @"Writer";
NSString *const MovieDetailActorsKey = @"Actors";
NSString *const MovieDetailPlotKey = @"Plot";
NSString *const MovieDetailLanguageKey = @"Language";
NSString *const MovieDetailCountryKey = @"Country";
NSString *const MovieDetailAwardsKey = @"Awards";
NSString *const MovieDetailPosterKey = @"Poster ";
NSString *const MovieDetailRatingsKey = @"Ratings ";
NSString *const MovieDetailMetaScoreKey = @"Metascore ";
NSString *const MovieDetailImdbRatingKey = @"imdbRating ";
NSString *const MovieDetailImdbVotesKey = @"imdbVotes ";
NSString *const MovieDetailImdbIDKey = @"imdbID ";
NSString *const MovieDetailTypeKey = @"Type ";
NSString *const MovieDetailDVDKey = @"DVD ";
NSString *const MovieDetailBoxOfficeKey = @"BoxOffice ";
NSString *const MovieDetailProductionKey = @"Production ";
NSString *const MovieDetailWebsiteKey = @"Website ";
NSString *const MovieDetailResponseKey = @"Response ";

