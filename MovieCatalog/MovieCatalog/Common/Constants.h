//
//  Constants.h
//  MovieCatalog
//
//  Created by Sunil Phani Manne on 11/01/19.
//  Copyright © 2019 Sunil Phani Manne. All rights reserved.
//

#import <CoreGraphics/CGBase.h>
#import <Foundation/Foundation.h>

//Useful Macros
#define  LIGHT_PURPLE_COLOR  [UIColor colorWithRed:180/255.0\
                                             green:138/255.0\
                                              blue:171/255.0\
                                             alpha:0.8]

#define MOVIE_DETAIL_CELL_SELECTION_COlOR LIGHT_PURPLE_COLOR

#define DEFAULT_MOVIE_POSTER_IMAGE [UIImage imageNamed:@"Movie.jpg"]

//Persistance Manager Constants
FOUNDATION_EXPORT NSString *const PersistanceManagerTestPrefixSeparator;

//Persistance Serial Queue
static const char *PersistanceSerialQueueName = "PersistanceSerialQueue";

//Miscellaneous
static const NSInteger BackgroundViewTag = 1010;
static const CGFloat MovieListTableViewCellHeight = 125.5;
static const NSInteger MovieListTableViewSectionCount = 1;
static const NSInteger MovieListPosterImageCacheInitialCapacity = 10;
static const NSInteger MovieDetailProcessedFieldsInitialCapacity = 10;

FOUNDATION_EXPORT NSString *const MovieDetailDateFormat;
FOUNDATION_EXPORT NSString *const MovieListCellNibName;
FOUNDATION_EXPORT NSString *const MovieDetailSegueIdentifier;
FOUNDATION_EXPORT NSString *const MovieListCellReuseIdentifier;
FOUNDATION_EXPORT NSString *const MovieDetailGenreDelimiter;
FOUNDATION_EXPORT NSString *const MovieDetailPersonDelimiter;

//JSON Releated Constants
FOUNDATION_EXPORT NSString *const MovieListJSONDownloadURL;
FOUNDATION_EXPORT NSString *const MovieListJSONParsingError;
FOUNDATION_EXPORT NSString *const MovieListJSONDownloadError;

//MovieList Model Object Keys
FOUNDATION_EXPORT NSString *const MovieListMovieListKey;
FOUNDATION_EXPORT NSString *const MovieListMoviesKey;

//MovieDetail Model Object Keys
FOUNDATION_EXPORT NSString *const MovieDetailTitleKey;
FOUNDATION_EXPORT NSString *const MovieDetailYearKey;
FOUNDATION_EXPORT NSString *const MovieDetailRatedKey;
FOUNDATION_EXPORT NSString *const MovieDetailReleasedKey;
FOUNDATION_EXPORT NSString *const MovieDetailRuntimeKey;
FOUNDATION_EXPORT NSString *const MovieDetailGenreKey;
FOUNDATION_EXPORT NSString *const MovieDetailDirectorKey;
FOUNDATION_EXPORT NSString *const MovieDetailWriterKey;
FOUNDATION_EXPORT NSString *const MovieDetailActorsKey;
FOUNDATION_EXPORT NSString *const MovieDetailPlotKey;
FOUNDATION_EXPORT NSString *const MovieDetailLanguageKey;
FOUNDATION_EXPORT NSString *const MovieDetailCountryKey;
FOUNDATION_EXPORT NSString *const MovieDetailAwardsKey;
FOUNDATION_EXPORT NSString *const MovieDetailPosterKey;
FOUNDATION_EXPORT NSString *const MovieDetailRatingsKey;
FOUNDATION_EXPORT NSString *const MovieDetailMetaScoreKey;
FOUNDATION_EXPORT NSString *const MovieDetailImdbRatingKey;
FOUNDATION_EXPORT NSString *const MovieDetailImdbVotesKey;
FOUNDATION_EXPORT NSString *const MovieDetailImdbIDKey;
FOUNDATION_EXPORT NSString *const MovieDetailTypeKey;
FOUNDATION_EXPORT NSString *const MovieDetailDVDKey;
FOUNDATION_EXPORT NSString *const MovieDetailBoxOfficeKey;
FOUNDATION_EXPORT NSString *const MovieDetailProductionKey;
FOUNDATION_EXPORT NSString *const MovieDetailWebsiteKey;
FOUNDATION_EXPORT NSString *const MovieDetailResponseKey;

/*!
 @typedef MediaType
 
 @brief  Enum containing the type of the media.
 
 @field MediaTypeMovie Indicates the media type is a movie.
 @field MediaTypeTVSeries  Indicates the media type is a TV series.
 */
typedef NS_ENUM(NSUInteger, MediaType) {
    MediaTypeMovie = 0,
    MediaTypeTVSeries
};

