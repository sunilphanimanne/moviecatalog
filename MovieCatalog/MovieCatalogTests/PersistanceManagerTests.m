//
//  PersistanceManagerTests.m
//  MovieCatalogTests
//
//  Created by Sunil Phani Manne on 16/01/19.
//  Copyright © 2019 Sunil Phani Manne. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "MovieList.h"
#import "PersistanceManager+Test.h"

@interface PersistanceManagerTests : XCTestCase

@end

@implementation PersistanceManagerTests

PersistanceManager *_systemUnderTest;

- (void)setUp {
    [super setUp];
    _systemUnderTest = [PersistanceManager sharedManager];
}

- (void)tearDown {
    _systemUnderTest = nil;
    [super tearDown];
}

- (MovieDetail *)getMovieDetailObject {
    MovieDetail *testObject = [[MovieDetail alloc] init];
    testObject.title = @"Test Title";
    testObject.year = @"2019";
    testObject.posterURL = [NSURL URLWithString:@"www.google.com"];
    
    return testObject;
}

- (MovieList *)getMovieListObject {
    MovieList *movieListObject = [[MovieList alloc] init];
    
    MovieDetail *movieDetailObject = [self getMovieDetailObject];
    [movieListObject addMovieDetail:movieDetailObject];
    
    return movieListObject;
}

- (void)testEffectivePrefix {
    _systemUnderTest.testNameSpacePrefix = @"Test";
    NSString *effectiveKey = [_systemUnderTest effectiveKeyForKey:@"helloworld"];
    XCTAssertEqualObjects(effectiveKey, @"Test.helloworld");
    
    _systemUnderTest.testNameSpacePrefix = @"Test";
    effectiveKey = [_systemUnderTest effectiveKeyForKey:@""];
    XCTAssertEqualObjects(effectiveKey, @"Test.");
    
    _systemUnderTest.testNameSpacePrefix = @"Test";
    effectiveKey = [_systemUnderTest effectiveKeyForKey:nil];
    XCTAssertNil(effectiveKey, @"Error: EffectiveKey should be nil.");
    
    _systemUnderTest.testNameSpacePrefix = @"";
    effectiveKey = [_systemUnderTest effectiveKeyForKey:@"helloworld"];
    XCTAssertEqualObjects(effectiveKey, @".helloworld");
    
    _systemUnderTest.testNameSpacePrefix = nil;
    effectiveKey = [_systemUnderTest effectiveKeyForKey:@"helloworld"];
    XCTAssertEqualObjects(effectiveKey, @"helloworld");
}

- (void)testPersistAndRetrieveObjectForMovieDetailObject
{
    NSString *key = @"PlainObject";
    MovieDetail *inputObject = [self getMovieDetailObject];
    
    //Persist the object...
    [self persistMovieDetailObject:inputObject forKey:key];
    
    //Read the object back...
    MovieDetail *outputObject = [self retrieveMovieDetailObjectForKey:key];
    
    [self assertMovieDetailObject1:inputObject withObject2:outputObject];
}

- (void)persistMovieDetailObject:(MovieDetail *)movieDetail forKey:(NSString *)key
{
    _systemUnderTest.testNameSpacePrefix = @"Test";
    
    //Persist the object
    [_systemUnderTest persistObject:movieDetail forKey:key];
}

- (MovieDetail *)retrieveMovieDetailObjectForKey:(NSString *)key
{
     return [_systemUnderTest retrieveObjectForKey:key];
}

- (void)deleteMovieDetailForKey:(NSString *)key
{
    //Delete the object for the key...
    [_systemUnderTest persistObject:nil forKey:key];
}

- (void)testPersistAndRetrieveObjectForMovieListObject
{
    NSString *key = @"CompositeObject";
    _systemUnderTest.testNameSpacePrefix = @"Test";
    MovieList *inputObject = [self getMovieListObject];
    
    //Persist the object
    [_systemUnderTest persistObject:inputObject forKey:key];
    
    //Read the object back...
    MovieList *outputObject = [_systemUnderTest retrieveObjectForKey:key];
    
    [self assertMovieListObject1:inputObject withObject2:outputObject];
    
    //Delete the object for the key...
    [_systemUnderTest persistObject:nil forKey:key];
}

- (void)assertMovieDetailObject1:(MovieDetail *)obj1 withObject2:(MovieDetail *)obj2
{
    XCTAssertEqualObjects(obj1.title, obj2.title);
    XCTAssertEqualObjects(obj1.year, obj2.year);
    XCTAssertEqualObjects(obj1.posterURL, obj2.posterURL);
}

- (void)assertMovieListObject1:(MovieList *)obj1 withObject2:(MovieList *)obj2
{
    MovieDetail *detail1 = nil;
    MovieDetail *detail2 = nil;
    
    if ([obj1.movieList count] > 0) {
        detail1 = obj1.movieList[0];
    }
    
    if ([obj2.movieList count] > 0) {
        detail2 = obj2.movieList[0];
    }
    
    [self assertMovieDetailObject1:detail1 withObject2:detail2];
}

- (void)testPerformanceOfPersistMovieDetailObject {
    NSString *key = @"PlainObject";
    
    MovieDetail *inputObject = [self getMovieDetailObject];
    
    [self measureBlock:^{
        //Persist the object...
        [self persistMovieDetailObject:inputObject forKey:key];
    }];
    
    [self deleteMovieDetailForKey:key];
}

- (void)testPerformanceOfRetrieveMovieDetailObject {
    NSString *key = @"PlainObject";
    
    MovieDetail *inputObject = [self getMovieDetailObject];
    __block MovieDetail *outputObject = nil;
    
    [self persistMovieDetailObject:inputObject forKey:key];
    
    [self measureBlock:^{
        //Retrieve the object...
        outputObject = [self retrieveMovieDetailObjectForKey:key];
    }];
    
    [self assertMovieDetailObject1:inputObject withObject2:outputObject];
    
    [self deleteMovieDetailForKey:key];
}


@end
