//
//  DataDownloaderTests.m
//  MovieCatalogTests
//
//  Created by Sunil Phani Manne on 17/01/19.
//  Copyright © 2019 Sunil Phani Manne. All rights reserved.
//

#import <XCTest/XCTest.h>

#import "Utils.h"
#import "Constants.h"
#import "DataDownloader.h"

static const NSInteger ExpectationWaitTimeInSeconds = 3;

@interface DataDownloaderTests : XCTestCase

@end

@implementation DataDownloaderTests

- (void)setUp {
    [super setUp];
}

- (void)tearDown {
    [super tearDown];
}

#pragma mark - Positive Tests

- (void)testDownloadLocalJSONPositiveScenario {
    NSString *localJSONPath = [[NSBundle mainBundle] pathForResource:@"MovieDetails" ofType:@"JSON"];
    localJSONPath = [NSString stringWithFormat:@"file://%@", localJSONPath];
    
    [self performPositiveDownloadTestForFilePath:localJSONPath];
}

- (void)testDownloadRemoteJSONPositiveScenario {
    NSString *remoteJSONPath = MovieListJSONDownloadURL;
    
    [self performPositiveDownloadTestForFilePath:remoteJSONPath];
}

#pragma mark - Negative Tests

- (void)testDownloadLocalJSONNegativeScenario {
    NSString *localJSONPath = [[NSBundle mainBundle] pathForResource:@"UnknownFile" ofType:@"JSON"];
    localJSONPath = [NSString stringWithFormat:@"file://%@", localJSONPath];
    
    [self performNegativeDownloadTestForFilePath:localJSONPath];
}

- (void)testDownloadRemoteJSONNegativeScenario {
    NSString *remoteJSONPath = @"http://unknownurl.com";
    
    [self performNegativeDownloadTestForFilePath:remoteJSONPath];
}

#pragma mark - Core test methods
- (void)performPositiveDownloadTestForFilePath:(NSString *)path
{
    XCTestExpectation* expectation = [self expectationWithDescription:@"Test DataDownloader"];
    NSURL *testURL = [NSURL URLWithString:path];
    
    [DataDownloader downloadJSONDataFromURL:testURL
                          completionHandler:^(NSDictionary *jsonDictionary, NSError *error) {
                              if (error) {
                                  XCTFail(@"Test failed with Error:%@", error.description);
                              }else {
                                  NSInteger movieCount = [jsonDictionary[MovieListMoviesKey] count];
                                  
                                  if (!error) {
                                      NSLog(@"Movies Found: %ld", movieCount);
                                      [expectation fulfill];
                                  }else {
                                      XCTFail(@"Test failed, invalid number of entries found:%ld", movieCount);
                                  }
                              }
    }];
    
    [self waitForExpectations:@[expectation] timeout:ExpectationWaitTimeInSeconds];
}

- (void)performNegativeDownloadTestForFilePath:(NSString *)path {
    XCTestExpectation* expectation = [self expectationWithDescription:@"Test DataDownloader"];
    NSURL *testURL = [NSURL URLWithString:path];
    
    [DataDownloader downloadJSONDataFromURL:testURL
                          completionHandler:^(NSDictionary *jsonDictionary, NSError *error) {
                              if (error) {
                                  NSLog(@"Error:%@", error);
                                  [expectation fulfill];
                              }else {
                                  XCTFail(@"Test failed, invalid filename given:%@", path);
                              }
    }];
    
    [self waitForExpectations:@[expectation] timeout:ExpectationWaitTimeInSeconds];
}

#pragma mark - Performance Tests

- (void)testPerformanceOfLocalJSONDownloadOperation {

    [self measureBlock:^{
        [self testDownloadLocalJSONPositiveScenario];
    }];
}

- (void)testPerformanceOfRemoteJSONDownloadOperation {
    
    [self measureBlock:^{
        [self testDownloadRemoteJSONPositiveScenario];
    }];
}

@end
